package com.mycompany.villagebutbetter;
import java.util.Scanner;

public class TestVillageProject{
	public static void main(String[] args) {
	Scanner pm = new Scanner (System.in);
	System.out.println("*-*-*-*-*-* Welcome to Village *-*-*-*-*-*");
	Villager peem = new Villager ("Peem",'M',"Agi");
        NormalHit skill1 = new NormalHit(peem.getAtk(),peem.getMp());
        FirstAid skill2 = new FirstAid(peem.getAtk(),peem.getMp());
        peem.showStatus();
        skill1.showDamage();
        skill2.showDamage();
        peem.hitDummy();
        peem.hitDummy(5);
       }   
}