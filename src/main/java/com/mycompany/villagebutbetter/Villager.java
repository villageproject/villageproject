/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.villagebutbetter;
import java.lang.Math;

/**
 *
 * @author BankMMT
 */
public class Villager { 
    protected String name;
    protected char gender;
    protected int str;
    protected int vit;
    protected int agi;
    protected int dex;
    protected int intel;
    protected int luk;
    protected int atk;
    protected int def;
    protected int hp;
    protected int mp;
    protected int atkSpeed;
    protected int acc;
    protected int cri;
    protected int eva;
    protected int moveSpeed = 1;
    protected String mainStatus;

    public Villager (String name,char gender,String mainStatus){
        this.name = name;
        this.gender = gender;
        this.mainStatus = mainStatus;
        calBaseStatus(this.mainStatus);

        System.out.println("Create "+name+" Success");
                
    }
    public String getName(){
        return name;
    }
    public char getGender(){
        return gender;
    }
    public int getAtk(){
        return atk;
    }
    public int getMp(){
        return mp;
    }
    public int randomBaseStatus(){
        double random = Math.random();
        int max = 15;
        int min = 1;
        int result = (int)(Math.random()*(max-min)+min);
        return result;
    }
    public void calBaseStatus(String mainStatus) {
        switch (mainStatus){
        case "Str" :
            vit = randomBaseStatus();
            agi = randomBaseStatus();
            dex = randomBaseStatus();
            intel = randomBaseStatus();
            luk = randomBaseStatus();
            str = 25;
            break;
        case "Vit" :
            str = randomBaseStatus();
            agi = randomBaseStatus();
            dex = randomBaseStatus();
            intel = randomBaseStatus();
            luk = randomBaseStatus();
            vit = 25;
            break;
        case "Agi" :
            vit = randomBaseStatus();
            str = randomBaseStatus();
            dex = randomBaseStatus();
            intel = randomBaseStatus();
            luk = randomBaseStatus();
            agi = 25;
            break;
        case "Dex" :
            vit = randomBaseStatus();
            agi = randomBaseStatus();
            str = randomBaseStatus();
            intel = randomBaseStatus();
            luk = randomBaseStatus();
            dex = 25;
            break;
        case "Int" :
            vit = randomBaseStatus();
            agi = randomBaseStatus();
            dex = randomBaseStatus();
            str = randomBaseStatus();
            luk = randomBaseStatus();
            intel = 25;
            break;
        case "Luk" :
            vit = randomBaseStatus();
            agi = randomBaseStatus();
            dex = randomBaseStatus();
            intel = randomBaseStatus();
            str = randomBaseStatus();
            luk = 25;
            break;
        default :
            System.out.println("Wrong Input!! ;-;");
            break;
        }
        calHp();
        calMp();
        calAtk();
        calDef();
        calAtkSpeed();
        calAcc();
        calCri();
        calEva();
    }
        public void showStatus(){
        System.out.println("Name   : "+name);
        if (gender == 'M' || gender == 'm'){
            System.out.println("Gender : Male");
        }else if (gender == 'F' || gender == 'f'){
            System.out.println("Gender : Female");
        }else {
            System.out.println("Unknow");
        }
        System.out.println("Str : "+str+"     (Strength)");
        System.out.println("Vit : "+vit+"     (Vitality)");
        System.out.println("Agi : "+agi+"     (Agility)");
        System.out.println("Dex : "+dex+"     (Dexterity)");
        System.out.println("Int : "+intel+"     (Intelligent)");
        System.out.println("Luk : "+luk+"     (Luck)");
        System.out.println("HP : "+hp);
        System.out.println("MP : "+mp);
        System.out.println("ATK : "+atk);
        System.out.println("DEF : "+def);
        System.out.println("AtkSpeed : "+atkSpeed);
        System.out.println("Accurency : "+acc);
        System.out.println("Critical chance : "+cri+("%"));
        System.out.println("Evasion chance : "+eva+("%"));
        System.out.println("-----------------------------------");
    }
    
    public void hitDummy(){
        System.out.println("Damage to Dummy : "+atk);
    }
    public void hitDummy(int time){
        System.out.println("Damage to Dummy : "+(atk*time));
    }
    public void calHp(){
        hp = 200+(vit*20);
    }
    public void calMp(){
        mp = 75+(intel*15);
    }
    public void calAtk(){
        atk = 20+(str*5)+(dex*2);
    }
    public void calDef(){
        def = 5+(vit*3);
    }
    public void calAtkSpeed(){
        atkSpeed = 20+(agi*5);
    }
    public void calAcc(){
        acc = 20+(dex*5);
    }
    public void calCri(){
        cri = (int)(luk*0.5);
    }
    public void calEva(){
        eva = (int)(agi*0.5);
    }


}