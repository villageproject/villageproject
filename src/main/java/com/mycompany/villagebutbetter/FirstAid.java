package com.mycompany.villagebutbetter;

public class FirstAid extends Skill {
	public FirstAid (int atk, int mana){
		super(atk,mana);
	}
	@Override
	public double damageTotal(){
		return (50+(atk/2));
	}
        @Override
        public void showDamage(){
        	System.out.println("Healing of FirstAid skill : "+damageTotal());
        }
}