package com.mycompany.villagebutbetter;

public class NormalHit extends Skill {
	public NormalHit (int atk, int mana){
		super(atk,mana);
	}
	@Override
	public double damageTotal(){
		return (atk*1.25);
	}
        @Override
        public void showDamage(){
        	System.out.println("Damage of Normalhit skill : "+damageTotal());
        }
}